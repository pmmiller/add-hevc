# Add HEVC
Powershell used to install HEVC (Manufactures version) from Windows Store

## Name
Add-HEVC.PS1

## Usage
The script has no logic because I use the RMM to check if files are already downloaded.
Update the _TempWork_ and _DownloadURL_ variables to suit your needs.

## Project status
The attached script is work in progress.  The cheat section works by downloading the files from my personal download storage.  I could not get the section above the cheat to work reliably because the site queried would not always be available.
